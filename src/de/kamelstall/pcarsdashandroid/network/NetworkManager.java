package de.kamelstall.pcarsdashandroid.network;

import java.io.IOException;
import java.util.LinkedList;

import javax.jmdns.JmDNS;
import javax.jmdns.ServiceEvent;
import javax.jmdns.ServiceListener;

import pcars.server.Constants;
import android.net.wifi.WifiManager.MulticastLock;
import android.os.AsyncTask;
import android.util.Log;

public class NetworkManager {
	public static final String TAG = "JmDNSResolver";

	/* Networking */
	private MulticastLock multicastLock;
	private JmDNS jmdns;
	private ServiceListener mServiceListener;

	private TheRealClient mTheClient;

	public NetworkManager() {
		Log.d(TAG, "remember to call setUp()");
	}
	
	
	public void setUp() {
		mTheClient = new TheRealClient();
		
		for (IPCarsDataReceiver r : allReceiver) {
			mTheClient.addDataReceiver(r);
		}

		mServiceListener = new ServiceListener() {
			public void serviceResolved(ServiceEvent ev) {
				Log.d(TAG, "Service resolved: " + ev.getInfo().getQualifiedName() + " port:" + ev.getInfo().getPort());
				mTheClient.setUp(ev.getInfo().getPort(), ev.getInfo().getInetAddress());
			}

			@Override
			public void serviceAdded(ServiceEvent event) {
				jmdns.requestServiceInfo(event.getType(), event.getName(), 1);
			}

			@Override
			public void serviceRemoved(ServiceEvent event) {
				mTheClient.tearDown();
			}
		};
		
		new ServerDetection().execute();
		/*
		try { 
			mTheClient.setUp(666, InetAddress.getByAddress(new byte[] { (byte)192,(byte) 168, (byte)178, (byte)107}));
			Log.d(TAG, "client setup");
		}
		catch (Exception e) {
			e.printStackTrace();
			Log.d(TAG, "client setup failed");
		}
		*/
		
		Log.d(TAG, "Resolver constructed!");
	}

	public void tearDown() {
		mTheClient.tearDown();
		
		if (multicastLock != null) {
			multicastLock.release();
		}

		if (jmdns != null) {
			try {
				jmdns.removeServiceListener(Constants.Comms.SERVICE_NAME, mServiceListener);
				jmdns.close();
			} catch (IOException ioe) {
				Log.d(TAG, "Failed to close JmDNS due to " + ioe.getMessage());
			}
		}
		Log.d(TAG, "Resolver teared down!");
	}

	private class ServerDetection extends AsyncTask<Void, Void, Void> {

		protected Void doInBackground(Void... params) {
			
			try {
				jmdns = JmDNS.create();

				jmdns.addServiceListener(Constants.Comms.SERVICE_NAME, mServiceListener);
				
				Log.d(TAG, "JmDNS initialized");
			} catch (IOException ioe) {
				Log.d(TAG, "Could not init JmDNS due to " + ioe.getMessage());
			}
			
			return null;
		}
	}
	
	LinkedList<IPCarsDataReceiver> allReceiver = new LinkedList<IPCarsDataReceiver>();
	public void addDataReceiver(IPCarsDataReceiver receiver) {
		if (receiver != null) {
			allReceiver.add(receiver);	
		}
	}

}
