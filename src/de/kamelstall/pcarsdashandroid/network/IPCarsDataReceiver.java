package de.kamelstall.pcarsdashandroid.network;

import pcars.server.PCarsData;

public interface IPCarsDataReceiver {

	public void onPCarsDataReceived(PCarsData data);
}
