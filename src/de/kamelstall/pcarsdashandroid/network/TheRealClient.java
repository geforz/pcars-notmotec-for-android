package de.kamelstall.pcarsdashandroid.network;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.LinkedList;

import pcars.server.Constants;
import pcars.server.PCarsData;
import android.os.AsyncTask;
import android.util.Log;

public class TheRealClient {
	public static final String TAG = "TheRealClient";

	private DatagramSocket theDataSocket;
	private Thread listenThread;
	private int mServerPort;
	private InetAddress mServerAddress;

	private boolean[] states;
	

	private int peerId = 255;
	
	

	public TheRealClient() {
		states = new boolean[Constants.MonitorStates.TOTAL_STATES];
		states[Constants.MonitorStates.GAME_STATE] = false;
		states[Constants.MonitorStates.UNFILTERED_INPUT] = false;
		states[Constants.MonitorStates.NAMES] = false;
		states[Constants.MonitorStates.TIMES] = true;
		states[Constants.MonitorStates.RACE_INFO] = false;
		states[Constants.MonitorStates.CAR_INFO] = true;
		states[Constants.MonitorStates.MOTION_AND_DEVICE] = false;
		states[Constants.MonitorStates.WHEELS_TYRES] = false;
	}
	

	public void setUp(int serverPort, InetAddress serverAddress) {
		mServerPort = serverPort;
		mServerAddress = serverAddress; 
		Log.d(TAG, "setUp a client with: " + serverPort + " and " + serverAddress);
		try {
			theDataSocket = new DatagramSocket();
			
			listenThread = new Thread(receivePacketRunnable);
			listenThread.start();

			
			ByteArrayOutputStream baos = new ByteArrayOutputStream(Constants.Comms.MAX_PACKET_SIZE);
			DataOutputStream dos = new DataOutputStream(baos);
				dos.writeInt(Constants.Comms.NEW_CONNECTION);
				dos.writeInt(Constants.Comms.NO_PEER_ID);
				for (boolean state : states) {
					dos.writeBoolean(state);					
				}
			dos.close();
			sendMessage(baos.toByteArray());
			Log.d(TAG, "NEW CONN MESSAGE SENT!");
		}
		catch (IOException ioe) {
			Log.d(TAG, "IOException: " + ioe.getMessage(), ioe);
		}
	}
	
	private void sendMessage(byte[] message) throws IOException {
		sendMessage(message, false);
	}
	private void sendMessage(byte[] message, boolean closeSocket) throws IOException {
		
		class Foo extends AsyncTask<Void, Void, Void> {

			private byte[] mMessage;
			private boolean mCloseSocket;
			public Foo(byte[] message, boolean closeSocket) {
				mMessage = message;
				mCloseSocket = closeSocket;
			}
			
			@Override
			protected Void doInBackground(Void... params) {
				
				DatagramPacket outboundPacket = new DatagramPacket(new byte[Constants.Comms.MAX_PACKET_SIZE], Constants.Comms.MAX_PACKET_SIZE);
				
				outboundPacket.setData(mMessage);
				outboundPacket.setPort(mServerPort);
		        outboundPacket.setAddress(mServerAddress);
		        try {
		        	theDataSocket.send(outboundPacket);
				} catch (IOException e) {
					Log.d(TAG, "Failed to send message " + e.getMessage());
				}
		        
		        if (mCloseSocket) {
		        	theDataSocket.close();
		        }
				
				return null;
			}
			
		}
		
		new Foo(message, closeSocket).execute();
	}
	
	public void tearDown() {
		if (theDataSocket != null) {
			
			try {
				ByteArrayOutputStream baos = new ByteArrayOutputStream(Constants.Comms.MAX_PACKET_SIZE);
				DataOutputStream dos = new DataOutputStream(baos);
					dos.writeInt(Constants.Comms.REMOVE_CONNECTION);
					dos.writeInt(peerId);
				dos.close();
				sendMessage(baos.toByteArray());
				Log.d(TAG, "REMOVE_CONNECTION MESSAGE SENT!");
			} catch (IOException e) {
			}
		}
	}

	/**
	 * Runnable to wait on the socket while receiving and processing incomming
	 * packets.
	 */
	private Runnable receivePacketRunnable = new Runnable() {
		@Override
		public void run() {
			Log.d(TAG, "Starting receiver");
			DatagramPacket incommingPacket = new DatagramPacket(new byte[Constants.Comms.MAX_PACKET_SIZE], Constants.Comms.MAX_PACKET_SIZE);
			while (!theDataSocket.isClosed()) {
				try {
					theDataSocket.receive(incommingPacket);
					// Got a new packet!
					processIncommmingPacket(incommingPacket);
				} catch (IOException e) {
					// IO error, let the thread run out
					Log.d(TAG, "error: " + e.getMessage(), e);
					e.printStackTrace();
					break;
				}
			}
			Log.d(TAG, "stopping receiver");
		}
	};

	/** Process the received packet from a peer */
	int p = 0;
	private void processIncommmingPacket(DatagramPacket packet) {
//		if (p % 10 != 0) {
//			return;
//		}
		// Open data streams to read the packet data
		ByteArrayInputStream bais = new ByteArrayInputStream(packet.getData());
		DataInputStream dis = new DataInputStream(bais);
		int type;
		try {
			// Get the type
			type = dis.readInt();
			//Log.d(TAG, "Got code: " + type);
			
			// ignore him :p
			
			// Process according to protocol
			switch (type) {
				case Constants.Comms.PING:
					peerId = dis.readInt();
					break;
				case Constants.Comms.PCARS_DATA:
					// update data for display
					peerId = dis.readInt();
					PCarsData pCarsData = PCarsData.readFromStrean(dis, states);
					synchronized (allReceiver) {
						for (IPCarsDataReceiver receiver : allReceiver) {
							receiver.onPCarsDataReceived(pCarsData);
						}	
					}
					break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	LinkedList<IPCarsDataReceiver> allReceiver = new LinkedList<IPCarsDataReceiver>();
	public void addDataReceiver(IPCarsDataReceiver receiver) {
		if (receiver != null) {
			synchronized (allReceiver) {
				allReceiver.add(receiver);	
			}
		}
	}

}
