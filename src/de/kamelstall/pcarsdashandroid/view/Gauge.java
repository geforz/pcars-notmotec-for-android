package de.kamelstall.pcarsdashandroid.view;

import java.util.Locale;

import pcars.server.PCarsData;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class Gauge extends View {

	public Gauge(Context context, AttributeSet attributes) {
		super(context, attributes);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		mode = (mode + 1) % NUM_MODES;
		return super.onTouchEvent(event);
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		int widthSize = MeasureSpec.getSize(widthMeasureSpec);
		Log.d("GAUGE", "width is:" + widthSize);

		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		int heightSize = MeasureSpec.getSize(heightMeasureSpec);
		Log.d("GAUGE", "height is:" + heightSize);

		int chosenWidth = chooseDimension(widthMode, widthSize);
		int chosenHeight = chooseDimension(heightMode, heightSize);

		float widthToHeightRation = 2;

		if (chosenWidth > chosenHeight * widthToHeightRation) {
			// height is the limit
			chosenWidth = (int) Math.ceil(chosenHeight * widthToHeightRation);
		} else {
			// width is the limit
			chosenHeight = (int) Math.ceil(chosenWidth / widthToHeightRation);
		}

		Log.d("GAUGE", "chosen dimension is is:" + chosenWidth + " by " + chosenHeight);

		setMeasuredDimension(chosenWidth, chosenHeight);
	}

	private int chooseDimension(int mode, int size) {
		if (mode == MeasureSpec.AT_MOST || mode == MeasureSpec.EXACTLY) {
			return size;
		} else { // (mode == MeasureSpec.UNSPECIFIED)
			return 300;
		}
	}

	float scale;
	// außenrahmen
	Paint paintBorder;
	Path borderPath;
	// innenbereich
	Paint paintInnerArea;
	Path innerAreaPath;
	// Texts
	Typeface lcdMonoTypeFace;
	Typeface lcdTypeFace;
	Paint paintLabelExtraSmall;
	Paint paintLabelSmall;
	Paint paintLabelMedium;
	Paint paintLabelLarge;
	Paint paintLabelExtraLarge;
	Paint paintLabelHuge;
	// RPMs
	Paint paintLine;
	Path RPMBarPath;
	Paint paintFilled;
	Path[] RPMBarPaths;
	int RPMBarParts = 70;

	final public int MODE_DEFAULT = 0;
	final public int MODE_TIMING = 1;
	final public int MODE_FUEL = 2;
	final public int MODE_TIRES = 3;
	final private int NUM_MODES = 4;

	int mode = MODE_DEFAULT;

	public void initializeDraw() {
		scale = (float) getWidth() / 1000;

		paintBorder = new Paint();
		paintBorder.setStyle(Style.FILL);
		paintBorder.setAntiAlias(true);
		paintBorder.setColor(Color.argb(255, 39, 37, 51));
		if (!isInEditMode()) {
			paintBorder.setShadowLayer(10, 2.5f, 2.5f, Color.BLACK);
		}

		// außenrahmen
		borderPath = new Path();
		borderPath.moveTo(500, 490);
		borderPath.lineTo(130, 490);
		borderPath.cubicTo(110, 490, 25, 490, 25, 390);
		borderPath.lineTo(25, 295);
		borderPath.cubicTo(25, 170, 200, 15, 500, 15);
		borderPath.cubicTo(800, 15, 975, 170, 975, 295);
		borderPath.lineTo(975, 390);
		borderPath.cubicTo(975, 490, 890, 490, 870, 490);
		borderPath.lineTo(500, 490);
		borderPath.close();

		// innenbereich
		paintInnerArea = new Paint();
		paintInnerArea.setStyle(Style.FILL);
		paintInnerArea.setAntiAlias(true);

		innerAreaPath = new Path();
		innerAreaPath.moveTo(500, 440);
		innerAreaPath.lineTo(130, 440);
		innerAreaPath.cubicTo(110, 440, 90, 425, 90, 390);
		innerAreaPath.lineTo(90, 290);
		innerAreaPath.cubicTo(90, 225, 215, 65, 500, 65);
		innerAreaPath.cubicTo(785, 65, 910, 225, 910, 290);
		innerAreaPath.lineTo(910, 390);
		innerAreaPath.cubicTo(910, 425, 890, 440, 870, 440);
		innerAreaPath.close();

		if (isInEditMode()) {
			lcdMonoTypeFace = Typeface.MONOSPACE;
			lcdTypeFace = Typeface.DEFAULT;
		} else {
			lcdMonoTypeFace = Typeface.createFromAsset(getContext().getAssets(), "fonts/digital-7 (mono).ttf");
			lcdTypeFace = Typeface.createFromAsset(getContext().getAssets(), "fonts/digital-7.ttf");
		}

		// Fonts
		paintLabelExtraSmall = new Paint();
		paintLabelExtraSmall.setTextAlign(Align.RIGHT);
		paintLabelExtraSmall.setTypeface(lcdMonoTypeFace);
		paintLabelExtraSmall.setTextSize(16);

		paintLabelSmall = new Paint();
		paintLabelSmall.setTextAlign(Align.RIGHT);
		paintLabelSmall.setTypeface(lcdMonoTypeFace);
		paintLabelSmall.setTextSize(23);

		paintLabelMedium = new Paint();
		paintLabelMedium.setTextAlign(Align.RIGHT);
		paintLabelMedium.setTypeface(lcdMonoTypeFace);
		paintLabelMedium.setTextSize(50);

		paintLabelLarge = new Paint();
		paintLabelLarge.setTextAlign(Align.RIGHT);
		paintLabelLarge.setTypeface(lcdMonoTypeFace);
		paintLabelLarge.setTextSize(80);

		paintLabelExtraLarge = new Paint();
		paintLabelExtraLarge.setTextAlign(Align.RIGHT);
		paintLabelExtraLarge.setTypeface(lcdMonoTypeFace);
		paintLabelExtraLarge.setTextSize(95);
		
		paintLabelHuge = new Paint();
		paintLabelHuge.setTextAlign(Align.RIGHT);
		paintLabelHuge.setTypeface(lcdMonoTypeFace);
		paintLabelHuge.setTextSize(160);

		// rpm
		paintLine = new Paint();
		paintLine.setStyle(Style.STROKE);
		paintLine.setColor(Color.BLACK);
		paintLine.setStrokeWidth(1);
		paintLine.setAntiAlias(true);

		/*
		 * float ax = 165; float ay = 355; float bx = 190; float by = 190; float
		 * cx = 465; float cy = 120; float dx = 690; float dy = 190;
		 */

		float ax = 140;
		float ay = 300;
		float bx = 230;
		float by = 150;
		float cx = 465;
		float cy = 130;
		float dx = 600;
		float dy = 160;

		RPMBarPath = new Path();
		RPMBarPath.moveTo(ax, ay);
		RPMBarPath.cubicTo(bx, by, cx, cy, dx, dy);

		paintFilled = new Paint(paintLine);
		paintFilled.setStyle(Style.FILL);

		// inner border of bars is at 0.02
		// outer border is between 0.1 and 0.11
		float inner = 0.02f;
		float outerMin = 0.05f;
		float outerMax = 0.08f;
		RPMBarPaths = new Path[RPMBarParts];

		for (int arc = 0; arc < RPMBarPaths.length; arc++) {
			float blFactor = (arc) / (float) RPMBarParts;
			float blPx = bezierPoint(blFactor, ax, bx, cx, dx);
			float blPy = bezierPoint(blFactor, ay, by, cy, dy);

			float blVx = bezierTangent(blFactor, ax, bx, cx, dx);
			float blVy = bezierTangent(blFactor, ay, by, cy, dy);

			float blSx = blVy;
			float blSy = -blVx;

			float blIx = blPx + ((inner) * blSx);
			float blIy = blPy + ((inner) * blSy);
			float blOx = blPx + ((inner + outerMin + outerMax * blFactor) * blSx);
			float blOy = blPy + ((inner + outerMin + outerMax * blFactor) * blSy);

			float buFactor = ((arc) + 1) / (float) RPMBarParts - 0.008f;
			float buPx = bezierPoint(buFactor, ax, bx, cx, dx);
			float buPy = bezierPoint(buFactor, ay, by, cy, dy);

			float buVx = bezierTangent(buFactor, ax, bx, cx, dx);
			float buVy = bezierTangent(buFactor, ay, by, cy, dy);

			float buSx = buVy;
			float buSy = -buVx;

			float buIx = buPx + ((inner) * buSx);
			float buIy = buPy + ((inner) * buSy);
			float buOx = buPx + ((inner + outerMin + outerMax * blFactor) * buSx);
			float buOy = buPy + ((inner + outerMin + outerMax * blFactor) * buSy);

			Path barPath = new Path();
			barPath.moveTo(blIx, blIy);
			barPath.lineTo(blOx, blOy);
			barPath.lineTo(buOx, buOy);
			barPath.lineTo(buIx, buIy);
			barPath.lineTo(blIx, blIy);
			barPath.close();

			RPMBarPaths[arc] = barPath;
		}
	}

	@Override
	protected void onDraw(Canvas canvas) {
		initializeDraw();

		// Log.d("GAUGE", "onDraw");
		super.onDraw(canvas);
		canvas.save(Canvas.MATRIX_SAVE_FLAG);
		canvas.scale(scale, scale);

		// outer border
		canvas.drawPath(borderPath, paintBorder);

		// background with "light"
		if (backgroundLightUp) {
			// light on
			paintInnerArea.setColor(Color.argb(255, 122, 142, 106));
		} else {
			// light off
			paintInnerArea.setColor(Color.argb(255, 0, 210, 211));
		}
		canvas.drawPath(innerAreaPath, paintInnerArea);

		// rpm line
		canvas.drawPath(RPMBarPath, paintLine);

		// rpm bars
		for (int b = 0; b <= numVisibleBars && b < RPMBarParts; b++) {
			canvas.drawPath(RPMBarPaths[b], paintFilled);
		}

		// always visibles
		canvas.drawText(labelSpeed, 850, 220, paintLabelSmall);
		canvas.drawText(labelUnits, 820, 200, paintLabelExtraSmall);
		canvas.drawText(valueSpeed, 780, 220, paintLabelLarge);
		canvas.drawText(labelGear, 480, 220, paintLabelSmall);
		canvas.drawText(valueGear, 420, 290, paintLabelHuge);
		// Values and Labels
		switch (mode) {
		case MODE_TIMING:
			// mode timing
			canvas.drawText(labelLaptime,	300,	290, paintLabelMedium);
			canvas.drawText(valueLaptime,	500,	370, paintLabelExtraLarge);
			canvas.drawText(labelSplit,		750,	290, paintLabelMedium);
			canvas.drawText(valueSplit,		900,	370, paintLabelExtraLarge);
			canvas.drawText(labelPosition,	230,	435, paintLabelLarge);
			canvas.drawText(valuePosition,	350,	435, paintLabelLarge);
			canvas.drawText(labelBestLap,	550,	435, paintLabelLarge);
			canvas.drawText(valueBestLap,	890,	435, paintLabelLarge);
			break;
		case MODE_FUEL:
			// mode fuel
			canvas.drawText(labelFuel, 300,	290, paintLabelMedium);
			canvas.drawText(valueFuel, 350, 370, paintLabelExtraLarge);
			canvas.drawText(labelLaps, 750,	290, paintLabelMedium);
			canvas.drawText(valueLaps, 750, 370, paintLabelExtraLarge);
			canvas.drawText(labelLast, 270, 435, paintLabelLarge);
			canvas.drawText(valueLast, 500, 435, paintLabelLarge);
			canvas.drawText(labelAvg, 700, 435, paintLabelLarge);
			canvas.drawText(valueAvg, 890, 435, paintLabelLarge);
			break;
		case MODE_TIRES:
			// mode tires
			canvas.drawText(labelTireTemps, 330, 290, paintLabelMedium);
			canvas.drawText(valueTireTempFrontLeft, 280, 370, paintLabelLarge);
			canvas.drawText(valueTireTempFrontRight, 430, 370, paintLabelLarge);
			canvas.drawText(valueTireTempRearLeft, 280, 435, paintLabelLarge);
			canvas.drawText(valueTireTempRearRight, 430, 435, paintLabelLarge);
			canvas.drawText(labelBrakeTemps, 800, 290, paintLabelMedium);
			canvas.drawText(valueBrakeTempFrontLeft, 730, 370, paintLabelLarge);
			canvas.drawText(valueBrakeTempFrontRight, 880, 370, paintLabelLarge);
			canvas.drawText(valueBrakeTempRearLeft, 730, 435, paintLabelLarge);
			canvas.drawText(valueBrakeTempRearRight, 880, 435, paintLabelLarge);
			break;
		case MODE_DEFAULT:
		default:
			// mode default
			canvas.drawText(labelBrakeBalance, 300, 290, paintLabelMedium);
			canvas.drawText(valueBrakeBalance, 330, 370, paintLabelExtraLarge);
			canvas.drawText(labelOilPressure, 550, 290, paintLabelMedium);
			canvas.drawText(valueOilPressure, 600, 370, paintLabelExtraLarge);
			canvas.drawText(labelRPM, 830, 290, paintLabelMedium);
			canvas.drawText(valueRPM, 900, 370, paintLabelExtraLarge);
			canvas.drawText(labelWaterTemp, 200, 435, paintLabelLarge);
			canvas.drawText(valueWaterTemp, 400, 435, paintLabelLarge);
			canvas.drawText(labelOilTemp, 700, 435, paintLabelLarge);
			canvas.drawText(valueOilTemp, 890, 435, paintLabelLarge);
			break;
		}
		canvas.restore();
	}

	private float bezierPoint(float t, float a, float b, float c, float d) {
		float C1 = (d - (3 * c) + (3 * b) - a);
		float C2 = ((3 * c) - (6 * b) + (3 * a));
		float C3 = ((3 * b) - (3 * a));
		float C4 = (a);

		return (C1 * t * t * t + C2 * t * t + C3 * t + C4);
	}

	float bezierTangent(float t, float a, float b, float c, float d) {
		float C1 = (d - (3 * c) + (3 * b) - a);
		float C2 = ((3 * c) - (6 * b) + (3 * a));
		float C3 = ((3 * b) - (3 * a));

		return ((3 * C1 * t * t) + (2 * C2 * t) + C3);
	}

	boolean backgroundLightUp = false;

	// Values and Labels
	// always visibles
	String labelSpeed = "SPEED";
	String labelUnits = "kmh";
	String valueSpeed = "888";
	String labelGear = "GEAR";
	String valueGear = "N";

	// mode default
	String labelBrakeBalance = "br_F";
	String valueBrakeBalance = "88.8";
	String labelOilPressure = "OILP";
	String valueOilPressure = "8.88";
	String labelRPM = "RPM";
	String valueRPM = "88888";
	String labelWaterTemp = "WT";
	String valueWaterTemp = "888";
	String labelOilTemp = "OILT";
	String valueOilTemp = "888";

	// mode timing
	String labelLaptime = "LAPT";
	String valueLaptime = "88:88:88";
	String labelSplit = "SPLIT";
	String valueSplit = "+88:88.88";
	String labelPosition = "POS";
	String valuePosition = "88";
	String labelBestLap = "BEST";
	String valueBestLap = "88:88:88";
	
	// mode fuel
	String labelFuel = "FUEL";
	String valueFuel = "88.8";
	String labelLaps = "LAPS";
	String valueLaps = "88/88";
	String labelLast = "LAST";
	String valueLast = "8.88";
	String labelAvg = "AVR";
	String valueAvg = "8.88";

	// mode tires
	String labelTireTemps = "TIRE_T";
	String valueTireTempFrontLeft = "888";
	String valueTireTempFrontRight = "888";
	String valueTireTempRearLeft = "888";
	String valueTireTempRearRight = "888";
	String labelBrakeTemps = "BRAKE_T";
	String valueBrakeTempFrontLeft = "888";
	String valueBrakeTempFrontRight = "888";
	String valueBrakeTempRearLeft = "888";
	String valueBrakeTempRearRight = "888";

	int numVisibleBars = RPMBarParts;

	public synchronized void updateData(PCarsData data) {
		// backgroundLightUp = data.getGear() > 2;
		backgroundLightUp = (data.getCarFlags() & PCarsData.CAR_FLAGS.CAR_HEADLIGHT) > 0;

		// always visible
		numVisibleBars = (int) Math.ceil(data.getRpm() / data.getMaxRPM() * RPMBarParts);
		valueSpeed = String.valueOf((int) Math.floor(data.getSpeed() * 3.6));
		valueGear = data.getGear() == 0 ? "N" : String.valueOf(data.getGear());

		// mode default
		valueBrakeBalance = String.format(Locale.US, "%.1f", data.getBrake()); //
		valueOilPressure = String.format(Locale.US, "%.2f", data.getOilPressureKPa()); // = "8.88";
		valueRPM = String.format(Locale.US, "%.1f", data.getRpm()); // = "88888";
		valueWaterTemp = String.format("%.0f", data.getWaterTempCelsius()); // = "888";
		valueOilTemp = String.format("%.0f", data.getOilTempCelsius()); // = "888";

		// mode timing
		valueLaptime = String.format(Locale.US, "%01.0f:%02.2f", data.getmCurrentTime() / 60, data.getmCurrentTime() % 60);
		valueSplit = String.format(Locale.US, "+%01.0f:%02.2f", data.getmSplitTime() / 60, data.getmSplitTime() % 60);
		valuePosition = String.format("%d", data.getCurrentPosition()); // = "88";
		valueBestLap = String.format(Locale.US, "%01.0f:%02.2f", data.getmBestLapTime() / 60, data.getmBestLapTime() % 60);
		
		// mode fuel
		valueFuel = String.format(Locale.US, "%.1f", data.getFuelLevel()); // = "88.8";
		valueLaps = String.format("%d/%d", data.getCurrentLap(), data.getLapsInEvent()); // = "88/88";
		valueLast = String.format(Locale.US, "%.2f", 8.88f); // = "8.88";
		valueAvg = String.format(Locale.US, "%.2f", 8.88f); // = "8.88";

		// mode tires
		if (data.getTyreTemp() != null) {
			valueTireTempFrontLeft = String.format("%.0f", data.getTyreTemp()[0]);
			valueTireTempFrontRight = String.format("%.0f", data.getTyreTemp()[1]);
			valueTireTempRearLeft = String.format("%.0f", data.getTyreTemp()[2]);
			valueTireTempRearRight = String.format("%.0f", data.getTyreTemp()[3]);
		}
		if (data.getBrakeDamage() != null) {
			valueBrakeTempFrontLeft = String.format("%.0f", data.getBrakeDamage()[0]);
			valueBrakeTempFrontRight = String.format("%.0f", data.getBrakeDamage()[1]);
			valueBrakeTempRearLeft = String.format("%.0f", data.getBrakeDamage()[2]);
			valueBrakeTempRearRight = String.format("%.0f", data.getBrakeDamage()[3]);
		}

		invalidate();
	}
}
